class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :token_password_reset
      t.datetime :time_token_password
      t.string :password_digest

      t.timestamps
    end
  end
end

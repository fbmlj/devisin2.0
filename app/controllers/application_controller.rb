class ApplicationController < ActionController::API

    protected
        def current_user
            header = request.headers['Authorization']
            header = header.split(' ').last if header
            
            return nil unless header.present?
            @decoded = JsonWebToken.decode(header)
            
            return nil unless @decoded
            user = User.find_by(@decoded[:user_id])
        end

        def must_exist_user
            if current_user.nil?
                render json: {mensage: "Você precisa está logado"},status: :forbidden
            end

        end
end

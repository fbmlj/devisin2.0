class RegisterController < ApplicationController
  def sign_up
    @user = User.new(register_params)
   

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def register_params

    params.require(:user).permit(:email, :name,:password,  :password_confirmation)
  end
end

class User < ApplicationRecord
    has_secure_password
    validates :name, presence: true, length: {maximum: 50}
    validates :password, presence: true, length: {minimum: 6}
    VALID_EMAIL_FORMAT= /\b[A-Z0-9._%a-z\-]+@injunior\.com\.br\z/
    validates :email, presence: true, length: {maximum: 260}, format: { with: VALID_EMAIL_FORMAT}, uniqueness: {case_sensitive: false}
    before_save { self.email = email.downcase }
end

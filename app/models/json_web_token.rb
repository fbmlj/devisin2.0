class JsonWebToken 

    def self.encode(payload)
        JWT.encode(payload, ENV["DEVISE_JWT_SECRET_KEY"])
        
    end

    def self.decode(token)
    begin
      
      decoded = JWT.decode(token, ENV["DEVISE_JWT_SECRET_KEY"])[0]
    rescue => exception
      return nil?
    end
    
  end
end